import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Tabs from './TabNavigation';
import Login from '../screens/Login';
import Intro from '../screens/Intro';
import Chart from '../screens/Charts';
import LineChart from '../screens/Charts/LineChart';

const Stack = createStackNavigator();

const MainNavigation = (props) => (
    <Stack.Navigator initialRouteName={(props.isLogin === 'true') ? 'Home' : ((props.intro == null) ? 'Intro' : 'Login')}>
        { props.intro == null && <Stack.Screen name="Intro" component={Intro} options={{ headerShown: false }} /> }
        <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
        <Stack.Screen name="Home" component={Tabs} options={{ headerShown: false }} />
        <Stack.Screen name="Chart" component={Chart} options={{ title: 'Jarak Tempuh Per Divisi' }} />
        <Stack.Screen name="LineChart" component={LineChart} options={{ title: 'Jarak Tempuh Per Divisi' }} />
    </Stack.Navigator>
)

export default MainNavigation;