import React, { useState, useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';
import SplashScreen from '../screens/SplashScreen';
import MainNav from './MainNavigation';
import { RootContext } from '../context';

function AppNavigation() {
    const [ isLoading, setIsLoading ] = useState(true)
    const [ intro, setIntro ] = useState(null)
    const [ isLogin, setIsLogin ] = useState('false')
    const [ isLoginChat, setIsLoginChat ] = useState('false')

    //mengatur durasi splashscreen saat aplikasi pertama kali dibuka
    useEffect(() => {
        setTimeout(() => {
            setIsLoading(!isLoading)
        }, 2000)

        async function getStatus() {
            const skipped = await AsyncStorage.getItem('skipped')
            setIntro(skipped)
        }

        const getLoginStatus = async () => {
            const statusLogin = await AsyncStorage.getItem('isLogin')
            setIsLogin(statusLogin)
        }

        const getLoginChatStatus = async () => {
            const statusLoginChat = await AsyncStorage.getItem('isLoginChat')
            changeStatusLoginChat(statusLoginChat)
        }

        getStatus()
        getLoginStatus()
        getLoginChatStatus()
    }, [])

    const changeStatusLoginChat = (status) => {
        setIsLoginChat(status)
    }

    if(isLoading) {
        return <SplashScreen />
    }

    return (
        <RootContext.Provider value={{ isLoginChat, changeStatusLoginChat }}>
        <NavigationContainer>
            <MainNav intro={intro} isLogin={isLogin}/>
        </NavigationContainer>
        </RootContext.Provider>
    )
}

export default AppNavigation;