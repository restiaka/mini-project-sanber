import React, { useContext } from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createStackNavigator } from '@react-navigation/stack';
import Schedule from '../screens/Schedule';
import Event from '../screens/Event';
import Profile from '../screens/Profile';
import Home from '../screens/Home';
import Maps from '../screens/Maps';
import Chat from '../screens/Chat/Chat';
import ChatLogin from '../screens/Chat';
import colors from '../style/colors';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { RootContext } from '../context';

const Stack = createStackNavigator();

const ChatNavigation = (props) => {
    const state = useContext(RootContext)
    
    return (
        <Stack.Navigator initialRouteName={(state.isLoginChat === 'true') ? 'Chat' : 'ChatLogin'}>
            <Stack.Screen name="ChatLogin" component={ChatLogin} options={{ headerShown: false }} />
            <Stack.Screen name="Chat" component={Chat} options={{ headerShown: false }} />
        </Stack.Navigator>
    )
}

const Tabs = createBottomTabNavigator();

const TabNavigation = ({route, navigation}) => {
    return (
        <Tabs.Navigator
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                    let iconName;

                    if (route.name === 'Schedule') {
                      iconName = focused
                        ? 'ios-calendar-outline'
                        : 'ios-calendar';
                    } else if (route.name === 'Profile') {
                      iconName = focused ? 'person-outline' : 'person';
                    } else if (route.name === 'Chat') {
                      iconName = focused ? 'chatbox-outline' : 'chatbox';
                    } else if (route.name === 'Dashboard') {
                      iconName = focused ? 'stats-chart-outline' : 'stats-chart';
                    } else if (route.name === 'Maps') {
                      iconName = focused ? 'map-outline' : 'map';
                    }

                    // You can return any component that you like here!
                    return <Ionicons name={iconName} size={size} color={color} />;
                },
            })}
            tabBarOptions={{
                inactiveBackgroundColor: colors.dark,
                activeTintColor: colors.danger,
                inactiveTintColor: 'gray',
            }}
        >
          <Tabs.Screen name="Dashboard" component={Home} />
          <Tabs.Screen name="Schedule" component={Event} />
          <Tabs.Screen name="Maps" component={Maps} />
          <Tabs.Screen name="Chat" component={ChatNavigation} />
          <Tabs.Screen name="Profile" component={Profile} />
        </Tabs.Navigator>
    );
}

export default TabNavigation;