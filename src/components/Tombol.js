import React from 'react';
import { StyleSheet, View, TouchableOpacity, Text } from 'react-native';


const Tombol = (props) => {
	return (
		<TouchableOpacity onPress={props.onPress} style={{flex:1}}>
	        <View style={[styles.btnContainer,{backgroundColor: (props.bgColor) ?  props.bgColor : '#203339'}]}>
	            <Text style={[styles.title,{color: (props.textColor) ?  props.textColor : 'white'}]}>
	                {props.title}
	            </Text>
	        </View>
	    </TouchableOpacity>
	)
}


const styles = StyleSheet.create({
	btnContainer: {
		margin: 4,
		height: 50,
		justifyContent: 'center',
		borderRadius: 3,
	},
	title: {
		textAlign: 'center',
		fontWeight: 'bold'
	}
});

export default Tombol;