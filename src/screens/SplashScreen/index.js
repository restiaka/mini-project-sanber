import React from 'react';
import { View, Image, StyleSheet } from "react-native";

function SplashScreen() {
    return (
        <View style={styles.container}>
            <View style={styles.logoContainer}>
                <Image style={{height: 200,width: 200}} source={require('../../assets/images/logo.jpg')} />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffffff'
    },
    logoContainer: {
        padding: 20,
        alignItems: 'center',
        justifyContent: "center"
    }
})

export default SplashScreen;