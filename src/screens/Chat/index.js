import React, { useState, useEffect, useContext } from 'react';
import {
    View,
    Text,
    Image,
    Button,
    TextInput,
    StatusBar,
    TouchableOpacity
} from 'react-native';
import auth from '@react-native-firebase/auth';
import styles from '../../style';
import colors from '../../style/colors';
import Tombol from '../../components/Tombol';
import AsyncStorage from '@react-native-community/async-storage';
import { RootContext } from '../../context';

function ChatLogin({ navigation }) {
    const state = useContext(RootContext)
    const [ email, setEmail ] = useState('')
    const [ password, setPassword ] = useState('')

    useEffect(() => {

    }, [])

    const changeLoginChatStatus = async (status) => {
        try {
            await AsyncStorage.setItem("isLoginChat", status)
            state.changeStatusLoginChat(status)
        } catch (err) {
            console.log(err)
        }
    }

    // login firebase chat
    const onLoginPress = () => {
        if (email == '' || password == '') {
            alert("Email dan Password harap diisi")
        } else {
            return auth().signInWithEmailAndPassword(email,password)
            .then((res) => {
                changeLoginChatStatus('true')
                navigation.reset({
                    index: 0,
                    routes: [{ name: 'Chat' }],
                });
            })
            .catch((err) => {
                alert(err)
            })
        }
    }

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor={colors.dark} barStyle="light-content" />
            <View style={styles.header}>
                <Text style={styles.userName}>
                    Join Group Chat
                </Text>
            </View>
            <View style={[styles.body,{height: 280}]}>
                <View style={{}}>
                    <Text style={styles.formLabel}>Email</Text>
                    <TextInput
                        value={email}
                        style={styles.textInput}
                        placeholder='Email Chat'
                        onChangeText={(email)=>{setEmail(email)}}
                    />
                    <Text style={styles.formLabel}>Password</Text>
                    <TextInput
                        value={password}
                        style={styles.textInput}
                        placeholder='Password Chat'
                        secureTextEntry={true}
                        onChangeText={(password)=>{setPassword(password)}}
                    />
                    <View style={styles.row}>
                        <Tombol onPress={()=>onLoginPress()} title={"Login Group Chat"} />
                    </View>
                </View>
                <View style={styles.loginFooter}>
                </View>
            </View>
        </View>
    )
}

export default ChatLogin;