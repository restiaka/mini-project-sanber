import React, { useEffect, useState } from 'react';
import { View, Text, StatusBar, TouchableOpacity } from 'react-native';
import { GiftedChat } from 'react-native-gifted-chat';
import styles from '../../style';
import colors from '../../style/colors';
import Tombol from '../../components/Tombol';
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';
import MCIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-community/async-storage';

const Chat = ({navigation}) => {
	const [ messages, setMessages ] = useState([])
	const [ user, setUser ] = useState([])
    const [ pp, setPP ] = useState(null)

	useEffect(() => {		
		const user = auth().currentUser
		setUser(user)
		getData()
		getPP()
		return () => {
			const db = database().ref('messages')
			if (db) {
				db.off()
			}
		}
	}, [])

	const getPP = async () => {
        const localPP = await AsyncStorage.getItem('local_pp')
        setPP(JSON.parse(localPP))
    }

	const getData = () => {
		database().ref('messages').limitToLast(20).on('child_added', snapshot => {
			const value = snapshot.val()
			setMessages(previousMessages => GiftedChat.append(previousMessages, value))
		})
	}

	// login firebase chat
    const onLogoutPress = () => {
        return auth().signOut()
        .then((res) => {
            navigation.navigate('ChatLogin')
        })
        .catch((err) => {
            alert(err)
        })
    }

	const onSend = (( messages = [] ) => {
		for (let i = 0; i < messages.length; i++) {
			database().ref('messages').push({
				_id: messages[i]._id,
				createdAt: database.ServerValue.TIMESTAMP,
				text: messages[i].text,
				user: messages[i].user,

			})
		}
	})

	return (
		<View style={styles.container}>
			<StatusBar backgroundColor={'#fff'} barStyle="dark-content" />
			<View style={{height: 60, backgroundColor: colors.dark,flexDirection: 'row',}}>
				<Text style={{flex:1, paddingLeft: 10, alignSelf: 'center', color: 'white',fontWeight: 'bold',fontSize: 20}}>
					{user && user.email}
				</Text>
				<TouchableOpacity onPress={() => onLogoutPress()} style={styles.btnLogoutChat}>
                    <MCIcons name="logout" size={35}></MCIcons>
                </TouchableOpacity>
			</View>
			<View style={{flex: 1}}>
				<GiftedChat
					messages={messages}
					onSend={messages => onSend(messages)}
					user={{
						_id: user.uid,
						name: user.email,
						avatar: (pp && pp.uri) ? pp.uri : "https://en.gravatar.com/userimage/6621161/0f56c6c0028f95d87823d03379d95062.jpeg"
					}}
				/>
			</View>
		</View>
	)
}

export default Chat;