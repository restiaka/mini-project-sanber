import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    Image,
    Button,
    TextInput,
    StatusBar,
    TouchableOpacity
} from 'react-native';
import styles from '../../style';
import colors from '../../style/colors';
import Tombol from '../../components/Tombol';
import Axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import api from '../../api';
import auth from '@react-native-firebase/auth';
import { GoogleSignin, statusCodes, GoogleSigninButton } from '@react-native-community/google-signin';
import TouchID from 'react-native-touch-id';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AntIcon from 'react-native-vector-icons/AntDesign';

const config = {
    title: 'Authentication Required',
    imageColor: colors.dark,
    imageErrorColor: colors.danger,
    sensorDescription: 'Touch Sensor',
    sensorErrorDescription: 'Failed',
    cancelText: 'Cancel'
}

function Login({ navigation }) {

    const [ email, setEmail ] = useState('')
    const [ password, setPassword ] = useState('')
    const [ pp, setPP ] = useState(null)

    useEffect(() => {
        configureGoogleSignin()
        const backToLogin = navigation.addListener('focus', () => {
            setFrontPP()
        });
    }, [])

    const setFrontPP = async () => {
        const localPP = await AsyncStorage.getItem('local_pp')
        setPP(JSON.parse(localPP))
    }

    const changeLoginStatus = async (status) => {
        try {
            await AsyncStorage.setItem("isLogin", status)
        } catch (err) {
            console.log(err)
        }
    }

    const saveToken = async (token) => {
        try {
            await AsyncStorage.setItem("token", token)
        } catch (err) {
            console.log(err)
        }
    }

    const configureGoogleSignin = () => {
        GoogleSignin.configure({
            offlineAccess: false,
            webClientId: '472949987951-dkok0v6j57q5o0ep6qff2rmihsau4ach.apps.googleusercontent.com'
        })
    }

    const signInWithGoogle = async () => {
        try {
            const { idToken } = await GoogleSignin.signIn()

            const credential = auth.GoogleAuthProvider.credential(idToken)

            auth().signInWithCredential(credential)

            changeLoginStatus('true')

            navigation.reset({
                index: 0,
                routes: [{ name: 'Home' }],
            });
        } catch (err) {
            alert('Login failed.\n\n'+err)
        }
    }

    const signInWithFingerprint = async () => {
        try {
            TouchID.authenticate('', config)
            .then(success => {
                changeLoginStatus('true')
                navigation.reset({
                    index: 0,
                    routes: [{ name: 'Home' }],
                });
            })
            .catch(error => {
                console.log("Authentication Failed")
            })
        } catch (err) {
            console.log("signInWithFingerprint -> err", err)
        }
    }

    const onLoginPress = () => {
        let data = {
            email: email,
            password: password
        }
        Axios.post(`${api}/login`, data, {
            timeout: 20000
        })
        .then((res)=>{
            saveToken(res.data.token)
            changeLoginStatus('true')
            navigation.reset({
                index: 0,
                routes: [{ name: 'Home' }],
            });
        })
        .catch((err)=>{
            alert('Login failed.\nPlease check your username and password!')
        })
    }

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor="#ffffff" barStyle="dark-content" />
            <View style={styles.loginPage}>
                <View style={styles.loginHeader}>
                    <Text style={{fontWeight: 'bold',fontSize: 30,}}>
                        INSTI GOWES
                    </Text>
                    <View style={styles.photoCircle}>
                        {(pp && pp.uri) ?
                            <Image style={{height:125,width:125,borderWidth: 2,borderRadius: 100,}} source={{uri: pp.uri}} />
                            :
                            <AntIcon name="user" size={50}></AntIcon>}
                    </View>
                </View>
                <View style={styles.loginBody}>
                    <Text style={styles.formLabel}>Username</Text>
                    <TextInput
                        value={email}
                        style={styles.textInput}
                        placeholder='Username or Email'
                        onChangeText={(email)=>{setEmail(email)}}
                    />
                    <Text style={styles.formLabel}>Password</Text>
                    <TextInput
                        value={password}
                        style={styles.textInput}
                        placeholder='Password'
                        secureTextEntry={true}
                        onChangeText={(password)=>{setPassword(password)}}
                    />
                    <View style={styles.row}>
                        <Tombol onPress={()=>onLoginPress()} title={"Login"} />
                        <TouchableOpacity onPress={()=>signInWithFingerprint()} style={styles.btnFinger}>
                            <Ionicons name="finger-print-outline" size={35}></Ionicons>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.hr}></View>
                    <Text style={styles.or}>OR</Text>
                    <GoogleSigninButton
                        onPress={() => signInWithGoogle()}
                        style={{width:'100%',height:55}}
                        size={GoogleSigninButton.Size.Wide}
                        color={GoogleSigninButton.Color.Dark}
                    />
                </View>
                <View style={styles.loginFooter}>
                </View>
            </View>
        </View>
    )
}

export default Login;