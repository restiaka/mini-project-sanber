import React from 'react';
import { View, Text, Image, StatusBar } from 'react-native';
import styles from '../../style';
import AppIntroSlider from 'react-native-app-intro-slider'; //import library atau module react-native-app-intro-slider
import Icon from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-community/async-storage';

//data yang akan digunakan dalam onboarding
const slides = [
    {
        key: 1,
        title: 'Verified Member',
        text: 'Informasi kegiatan bersepeda para pegawai\nRSUP Dr. Sardjito',
        image: require('../../assets/images/logo.jpg'),
    },
    {
        key: 2,
        title: 'Bike To Work',
        text: 'Event utama yang rutin diselenggarakan 2 kali setiap pekan',
        image: require('../../assets/images/bike_to_work.png'),
    },
    {
        key: 3,
        title: 'Synchronized',
        text: 'Temukan peserta lain dengan jadwal dan destinasi gowes yang sama',
        image: require('../../assets/images/location-icon.jpg'),
    },
];

const Intro = ({ navigation }) => {

    //menampilkan data slides kedalam renderItem
    const renderItem = ({ item }) => {
        return (
            <View style={styles.slide}>
                <Text style={styles.title}>{item.title}</Text>
                <Image source={item.image} style={styles.image} />
                <Text style={styles.text}>{item.text}</Text>
            </View>
        );
    }

    //fungsi ketika onboarding ada di list terakhir atau screen terakhir / ketika button done di klik
    const onDone = async () => {
        await AsyncStorage.setItem('skipped','true')
        navigation.navigate('Login')
    }

    //mengcustom tampilan button done
    const renderDoneButton = () => {
        return (
            <View style={styles.buttonCircle}>
                <Icon
                    name="md-checkmark"
                    color="rgba(255, 255, 255, .9)"
                    size={24}
                />
            </View>
        );
    };

    //mengcustom tampilan next button
    const renderNextButton = () => {
        return (
            <View style={styles.buttonCircle}>
                <Icon
                    name="arrow-forward"
                    color="rgba(255, 255, 255, .9)"
                    size={24}
                />
            </View>
        );
    };

    return (
        <View style={styles.container}>
            <StatusBar barStyle="dark-content" backgroundColor="#ffffff" />
            <View style={{ flex: 1 }}>
                {/* merender atau menjalankan library react-native-app-intro-slider */}
                <AppIntroSlider
                    data={slides}
                    onDone={onDone}
                    renderItem={renderItem}
                    renderDoneButton={renderDoneButton}
                    renderNextButton={renderNextButton}
                    keyExtractor={(item, index) => index.toString()}
                    activeDotStyle={{ backgroundColor: '#191970' }}
                />
            </View>
        </View>
    )
}

export default Intro