import React, { useEffect, useState } from 'react';
import { View, Text, StatusBar, TouchableOpacity, StyleSheet, FlatList, TextInput, Button } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import styles from '../../style';
import colors from '../../style/colors';
import Tombol from '../../components/Tombol';
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';
import MCIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-community/async-storage';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Icon from 'react-native-vector-icons/AntDesign';
import Modal from 'react-native-modal';

const Event = ({navigation}) => {
	const [ kegiatan, setKegiatan ] = useState([])
	const [ user, setUser ] = useState([])
	const [ judul, setJudul ] = useState('')
	const [ waktu, setWaktu ] = useState('')
	const [ refresh, setRefresh ] = useState(true)
	const [ isVisible, setIsVisible ] = useState(false)
	const [ selectedEvent, setSelectedEvent ] = useState([])
	const [ daftarPeserta, setDaftarPeserta ] = useState([])

	useEffect(() => {		
		const user = auth().currentUser
		setUser(user)
        getData()
		return () => {
			const db = database().ref('kegiatan')
			if (db) {
				db.off()
			}
		}
	}, [daftarPeserta])

	const getData = () => {
		let test = []
		database().ref('kegiatan').limitToLast(20).on('child_added', (snapshot, prevChildKey) => {
			const value = snapshot.val()
			const data = value.data
			data['key'] = snapshot.key
			test = [...test, data]
			setKegiatan(test)
		})
	}

	const addKegiatan = () => {
		if (judul === '') {
			alert('Inputkan judul event')
		} else if (waktu === '') {
			alert('Inputkan waktu pelaksanaan event')
		} else {
			let newKegiatan = [...kegiatan, {judul: judul, waktu: waktu}]
			setKegiatan(newKegiatan)
			saveKegiatan(newKegiatan)
			getData()
		}
	}

	const deleteKegiatan = (index) => {
		// kegiatan.splice(index, 1)
		// saveKegiatan(kegiatan)
		setIsVisible(true)
		setRefresh(!refresh)
	}

	const addPeserta = () => {
		let member = selectedEvent.member
		if (member) {
			member.push(user.email)
		} else {
			member = [user.email]
		}
		member = [...new Set(member)]
		var kegiatanRef = database().ref('kegiatan/'+selectedEvent.key+'/data');
		kegiatanRef.update({
			"member": member
		});

		setDaftarPeserta(member)
		setRefresh(!refresh)
	}

	const removePeserta = (key,member) => {
		var items = selectedEvent.member
		const filteredItems = items.filter(item => item !== user.email)
		var kegiatanRef = database().ref('kegiatan/'+selectedEvent.key+'/data');
		kegiatanRef.update({
			"member": filteredItems
		});
		setDaftarPeserta(filteredItems)
		setRefresh(!refresh)
	}

	const saveKegiatan = (( kegiatan = [] ) => {
		database().ref('kegiatan').push({
			createdAt: database.ServerValue.TIMESTAMP,
			data: {judul: judul, waktu: waktu}
		})
	})

	const detilKegiatan = (singleEvent) => {
		setSelectedEvent(singleEvent)
		setDaftarPeserta(singleEvent.member)
		setIsVisible(true)
	}

	const renderItem = ({item,index}) => {
		return (
			<View style={local_styles.list}>
				<View>
					<Text>{item.judul}</Text>
					<Text>{item.waktu}</Text>
				</View>
				<TouchableOpacity style={local_styles.deleteButton} onPress={() => detilKegiatan(item)}>
					<MCIcons name="book-information-variant" size={25} />
				</TouchableOpacity>
			</View>
		)
	}

	const renderItemPeserta = ({item,index}) => {
		return (
			<View style={local_styles.list}>
				<View>
					<Text>{item}</Text>
				</View>
				{ item == user.email && 
					<TouchableOpacity style={local_styles.deleteButton} onPress={() => removePeserta()}>
						<Icon name="delete" size={25} />
					</TouchableOpacity>
				}
			</View>
		)
	}

	const modalDetail = () => {
		console.log("daftarPeserta",daftarPeserta)
		return (
			<View>				
				<Modal
			        backdropOpacity={0.5}
			        animationIn="slideInLeft"
			        animationOut="slideOutRight"
			        isVisible={isVisible}>
					<View style={local_styles.modalContent}>
						<View style={local_styles.modalBody}>
							<Text style={local_styles.modalContentTitle}>{selectedEvent.judul}</Text>
							<Text>{selectedEvent.waktu}</Text>
							<Text>Peserta :</Text>
						</View>
						<View style={local_styles.listContainerPeserta}>
							<FlatList
								data={daftarPeserta}
								renderItem={renderItemPeserta}
								keyExtractor={(item,index)=>index.toString()}
								extraData={refresh}
							/>
						</View>
						<View style={local_styles.btnGroup}>
							<Tombol onPress={() => addPeserta()} title={"Hadir"} bgColor={colors.success} />
							<Tombol onPress={() => toggleModal()} title={"Close"} />
						</View>
					</View>
				</Modal>
			</View>
		)
	}

	const toggleModal = () => {
		setIsVisible(!isVisible);
	};

	return (
		<View style={styles.container}>
			<StatusBar backgroundColor="#203339" barStyle="light-content" />
			{modalDetail()}
			<View style={local_styles.formContainer}>
				<Text style={{marginBottom: 10,fontWeight: 'bold',fontSize: 30,alignSelf: 'center'}}>Event Gowes</Text>
				<View>
					<Text style={styles.formLabel}>Judul</Text>
					<TextInput
						style={styles.textInput}
						value={judul}
						placeholder="Inputkan judul event"
						onChangeText={(judul) => setJudul(judul)}
					/>
				</View>
				<View>
					<Text style={styles.formLabel}>Tanggal / Jam Start</Text>
					<TextInput
						style={styles.textInput}
						value={waktu}
						placeholder="Inputkan waktu pelaksanaan event"
						onChangeText={(waktu) => setWaktu(waktu)}
					/>
				</View>
				<Tombol onPress={() => addKegiatan()} title={"Tambah"} />
			</View>
			<View style={local_styles.listContainer}>
				<FlatList
					data={kegiatan}
					renderItem={renderItem}
					keyExtractor={(item,index)=>index.toString()}
					extraData={refresh}
				/>
			</View>
		</View>
	)
}

const local_styles = StyleSheet.create({
	formContainer: {
		marginTop: 10,
		padding: 20,
		height: 300,
	},
	listContainer: {
		flex: 1,
		paddingHorizontal: 10,
	},
	listContainerPeserta: {
		flex: 1,
	},
	list: {
		padding: 20,
	    paddingRight: 100,
	    borderWidth: 5,
	    borderRadius: 5,
	    borderColor: '#ededed',
	    marginBottom: 10
	},
	deleteButton: {
		position: 'absolute',
	    justifyContent: 'center',
	    alignItems: 'center',
	    padding: 10,
	    top: 10,
	    bottom: 10,
	    right: 10
	},
	modalContent: {
		height: hp('80%'),
		backgroundColor: 'white',
		padding: 22,
		justifyContent: 'center',
		borderRadius: 4,
		borderColor: 'rgba(0, 0, 0, 0.1)',
	},
	modalContentTitle: {
		fontWeight: 'bold',
		fontSize: 20,
		marginBottom: 12,
	},
	btnGroup: {
		alignSelf: 'stretch',
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
	modalBody: {
		paddingHorizontal: 5,
		marginBottom: 10,
	}
});

export default Event;