import React from 'react';
import { View, Text, StyleSheet, StatusBar, Image, ScrollView, TouchableOpacity } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MCIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import colors from '../../style/colors';

const Home = ({navigation}) => {
	return (
		<ScrollView>
		<View style={styles.container}>
            <StatusBar backgroundColor="#ffffff" barStyle="dark-content" />
			
			<View style={styles.card}>
				<View style={[styles.cardFooter,{backgroundColor: colors.dark,borderTopRightRadius: 50,}]}>
					<View style={styles.row}>
						<TouchableOpacity onPress={() => navigation.navigate('Chart')}>
							<View style={styles.divIcon}>
								<Ionicons name="bicycle" size={wp('17%')} color={colors.info} />
								<Text style={[styles.normalText,{color: colors.info}]}>Bar Chart Jarak</Text>
							</View>
						</TouchableOpacity>
						<TouchableOpacity onPress={() => navigation.navigate('LineChart')}>
							<View style={styles.divIcon}>
								<Ionicons name="analytics-outline" size={wp('17%')} color={colors.info} />
								<Text style={[styles.normalText,{color: colors.info}]}>Line Chart Jarak</Text>
							</View>
						</TouchableOpacity>
					</View>
				</View>
			</View>

			<View style={styles.card}>
				<View style={styles.cardHeader}>
					<Text style={styles.cardTitle}>Pencapaian Km Divisi INSTI</Text>
				</View>
				<View style={styles.cardListLight}>
					<Text style={styles.boldText}>Januari</Text>
				</View>
				<View style={styles.cardListDark}>
					<View style={styles.rowList}>
						<Text style={styles.colText}>Analis</Text>
						<Text style={styles.colTextRight}>0 Km</Text>
					</View>
					<View style={styles.rowList}>
						<Text style={styles.colText}>Programmer</Text>
						<Text style={styles.colTextRight}>0 Km</Text>
					</View>
					<View style={styles.rowList}>
						<Text style={styles.colText}>System Support</Text>
						<Text style={styles.colTextRight}>0 Km</Text>
					</View>
					<View style={styles.rowList}>
						<Text style={styles.colText}>Total</Text>
						<Text style={styles.colTextRight}>0 Km</Text>
					</View>
				</View>

				<View style={styles.cardListLight}>
					<Text style={styles.boldText}>Februari</Text>
				</View>
				<View style={styles.cardListDark}>
					<View style={styles.rowList}>
						<Text style={styles.colText}>Analis</Text>
						<Text style={styles.colTextRight}>40 Km</Text>
					</View>
					<View style={styles.rowList}>
						<Text style={styles.colText}>Programmer</Text>
						<Text style={styles.colTextRight}>30 Km</Text>
					</View>
					<View style={styles.rowList}>
						<Text style={styles.colText}>System Support</Text>
						<Text style={styles.colTextRight}>20 Km</Text>
					</View>
					<View style={styles.rowList}>
						<Text style={styles.colText}>Total</Text>
						<Text style={styles.colTextRight}>90 Km</Text>
					</View>
				</View>

				<View style={styles.cardListLight}>
					<Text style={styles.boldText}>Maret</Text>
				</View>
				<View style={styles.cardListDark}>
					<View style={styles.rowList}>
						<Text style={styles.colText}>Analis</Text>
						<Text style={styles.colTextRight}>10 Km</Text>
					</View>
					<View style={styles.rowList}>
						<Text style={styles.colText}>Programmer</Text>
						<Text style={styles.colTextRight}>20 Km</Text>
					</View>
					<View style={styles.rowList}>
						<Text style={styles.colText}>System Support</Text>
						<Text style={styles.colTextRight}>10 Km</Text>
					</View>
					<View style={styles.rowList}>
						<Text style={styles.colText}>Total</Text>
						<Text style={styles.colTextRight}>40 Km</Text>
					</View>
				</View>

				<View style={styles.cardListLight}>
					<Text style={styles.boldText}>April</Text>
				</View>
				<View style={styles.cardListDark}>
					<View style={styles.rowList}>
						<Text style={styles.colText}>Analis</Text>
						<Text style={styles.colTextRight}>30 Km</Text>
					</View>
					<View style={styles.rowList}>
						<Text style={styles.colText}>Programmer</Text>
						<Text style={styles.colTextRight}>20 Km</Text>
					</View>
					<View style={styles.rowList}>
						<Text style={styles.colText}>System Support</Text>
						<Text style={styles.colTextRight}>50 Km</Text>
					</View>
					<View style={styles.rowList}>
						<Text style={styles.colText}>Total</Text>
						<Text style={styles.colTextRight}>100 Km</Text>
					</View>
				</View>

				<View style={styles.cardListLight}>
					<Text style={styles.boldText}>Mei</Text>
				</View>
				<View style={styles.cardListDark}>
					<View style={styles.rowList}>
						<Text style={styles.colText}>Analis</Text>
						<Text style={styles.colTextRight}>60 Km</Text>
					</View>
					<View style={styles.rowList}>
						<Text style={styles.colText}>Programmer</Text>
						<Text style={styles.colTextRight}>10 Km</Text>
					</View>
					<View style={styles.rowList}>
						<Text style={styles.colText}>System Support</Text>
						<Text style={styles.colTextRight}>20 Km</Text>
					</View>
					<View style={styles.rowList}>
						<Text style={styles.colText}>Total</Text>
						<Text style={styles.colTextRight}>90 Km</Text>
					</View>
				</View>

				<View style={styles.cardListLight}>
					<Text style={styles.boldText}>Juni</Text>
				</View>
				<View style={styles.cardListDark}>
					<View style={styles.rowList}>
						<Text style={styles.colText}>Analis</Text>
						<Text style={styles.colTextRight}>20 Km</Text>
					</View>
					<View style={styles.rowList}>
						<Text style={styles.colText}>Programmer</Text>
						<Text style={styles.colTextRight}>50 Km</Text>
					</View>
					<View style={styles.rowList}>
						<Text style={styles.colText}>System Support</Text>
						<Text style={styles.colTextRight}>30 Km</Text>
					</View>
					<View style={styles.rowList}>
						<Text style={styles.colText}>Total</Text>
						<Text style={styles.colTextRight}>100 Km</Text>
					</View>
				</View>

				<View style={styles.cardFooter}>
				</View>
			</View>
		</View>
		</ScrollView>
	)
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingHorizontal: wp('3%'),
		paddingTop: hp('2%'),
		backgroundColor: 'white',
	},
	card: {
		marginBottom: hp('2%'),
	},
	cardHeader: {
		backgroundColor: colors.dark,
		borderTopLeftRadius: wp('2%'),
		borderTopRightRadius: wp('2%'),
		paddingVertical: wp('1%'),
		paddingHorizontal: wp('2%'),
	},
	cardListLight: {
		padding: wp('2%'),
		backgroundColor: colors.danger,
	},
	cardListDark: {
		padding: wp('2%'),
		backgroundColor: colors.primary,
	},
	cardFooter: {
		backgroundColor: colors.primary,
		paddingVertical: wp('1%'),
		paddingHorizontal: wp('2%'),
		borderBottomLeftRadius: wp('2%'),
		borderBottomRightRadius: wp('2%'),
	},
	cardTitle: {
		color: '#ffffff',
		fontSize: wp('5%') // End result looks like the provided UI mockup
	},
	divIcon: {
		alignItems: 'center'
	},
	imageIcon: {
		width: wp('18%'),
		height: hp('9.5%'),
	},
	normalText: {
		color: '#ffffff',
		fontSize: wp('4%'),
	},
	colText: {
		color: colors.dark,
		fontSize: wp('4%'),
		width: wp('40%'),
	},
	colTextRight: {
		color: colors.dark,
		fontSize: wp('4%'),
		width: wp('40%'),
		textAlign: 'right',
	},
	boldText: {
		color: '#ffffff',
		fontSize: wp('4%'),
		fontWeight: 'bold',
	},
	row: {
		flexDirection: 'row',
		justifyContent: 'space-around',
	},
	rowList: {
		flexDirection: 'row',
		justifyContent: 'space-around',
	}
});

export default Home;