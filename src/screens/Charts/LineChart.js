import React from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  Button,
  View, processColor
} from 'react-native';
import colors from '../../style/colors';

import {LineChart} from 'react-native-charts-wrapper';

class LineChartScreen extends React.Component {

  constructor() {
    super();

    this.state = {
      data: {
      dataSets: [{
        values: [
          {x: 1, y: 0},
          {x: 2, y: 40},
          {x: 3, y: 10},
          {x: 4, y: 30},
          {x: 5, y: 60},
          {x: 6, y: 20}
        ],
        label: 'Analis',
        config: {
          colors: [processColor(colors.primary)],
        }
      }, {
        values: [
          {x: 1, y: 0},
          {x: 2, y: 30},
          {x: 3, y: 20},
          {x: 4, y: 20},
          {x: 5, y: 10},
          {x: 6, y: 50}
        ],
        label: 'Programmer',
        config: {
          colors: [processColor(colors.info)],
        }
      }, {
        values: [
          {x: 1, y: 0},
          {x: 2, y: 20},
          {x: 3, y: 10},
          {x: 4, y: 50},
          {x: 5, y: 20},
          {x: 6, y: 30}
        ],
        label: 'System Support',
        config: {
          colors: [processColor(colors.warning)],
        }
      }],
    },

      marker: {
        enabled: true,
        digits: 2,
        backgroundTint: processColor('teal'),
        markerColor: processColor('#F0C0FF8C'),
        textColor: processColor('white'),
      },
      xAxis: {
        granularityEnabled: true,
        granularity: 1,
      },
      // visibleRange: {x: {min: 1, max: 2}}
    };
  }

  render() {
    return (
      <View style={{flex: 1}}>

        <View style={styles.container}>
          <LineChart
            style={styles.chart}
            data={this.state.data}
            chartDescription={{text: ''}}
            legend={this.state.legend}
            marker={this.state.marker}
            xAxis={this.state.xAxis}            
            drawGridBackground={false}
            borderColor={processColor('teal')}
            borderWidth={1}
            drawBorders={true}
            autoScaleMinMaxEnabled={false}
            touchEnabled={true}
            dragEnabled={true}
            scaleEnabled={false}
            scaleXEnabled={false}
            scaleYEnabled={false}
            pinchZoom={false}
            doubleTapToZoomEnabled={true}
            highlightPerTapEnabled={true}
            highlightPerDragEnabled={false}
            // visibleRange={this.state.visibleRange}
            dragDecelerationEnabled={true}
            dragDecelerationFrictionCoef={0.99}
            ref="chart"
            keepPositionOnRotation={false}
            // onSelect={this.handleSelect.bind(this)}
            // onChange={(event) => console.log(event.nativeEvent)}
          />
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF'
  },
  chart: {
    flex: 1
  }
});

export default LineChartScreen;