import React, { useState } from 'react';
import {
	StyleSheet,
	Text,
	View, processColor
} from 'react-native';
import colors from '../../style/colors';
import {BarChart} from 'react-native-charts-wrapper';

const StackedBarChart = () => {

	const [ localState, setLocalState ] = useState({
		legend: {
			enabled: true,
			textSize: 14,
			form: "SQUARE",
			formSize: 14,
			xEntrySpace: 10,
			yEntrySpace: 5,
		    formToTextSpace: 5,
		    wordWrapEnabled: true,
		    maxSizePercent: 0.5
		},
		data: {
			dataSets: [{
				values: [
						{y:[0, 0, 0]},
						{y:[40, 30, 20], marker: ["40 Km", " 30 Km", "20 Km"]},
						{y:[10, 20, 10], marker: ["10 Km", " 20 Km", "10 Km"]},
						{y:[30, 20, 50], marker: ["30 Km", " 20 Km", "50 Km"]},
						{y:[60, 10, 20], marker: ["60 Km", " 10 Km", "30 Km"]},
						{y:[20, 50, 30], marker: ["20 Km", " 50 Km", "30 Km"]},
						],
				label: '',
				config: {
					colors: [processColor(colors.info), processColor(colors.warning), processColor(colors.primary)],
					stackLabels: ['Programmer', 'System Support', 'Analis'],
					drawFilled: false,
	                drawValues: false,
				}
			}],
		},
		highlights: [{x: 3, stackIndex: 2}, {x: 5, stackIndex: 1}, {x: 4, stackIndex: 0}],
		xAxis: {
			valueFormatter: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'],
			granularityEnabled: true,
			granularity: 1,
			position: 'BOTTOM',
			drawGridLines: false,
		    axisMinimum: -0.5,
		    granularityEnabled: true,
		    granularity: 1,
		    axisMaximum: new Date().getMonth() + 0.5,
		},
		yAxis: {
			left: {
		        axisMinimum: 0,
		        labelCountForce: true,
		        granularity: 5,
		        granularityEnabled: true,
		        drawGridLines: true
		    },
		    right: {
		        axisMinimum: 0,
		        labelCountForce: true,
		        granularity: 5,
		        granularityEnabled: true,
		        enabled: true
		    }
		}

	});

	const handleSelect = (event) => {
		let entry = event.nativeEvent
		if (entry == null) {
			setLocalState({...localState, selectedEntry: null})
		} else {
			setLocalState({...localState, selectedEntry: JSON.stringify(entry)})
		}

		// console.log(event.nativeEvent)
	}

	return (

		<View style={{flex: 1}}>

			{/*<View style={{height:80}}>
				<Text> selected entry</Text>
				<Text> {localState.selectedEntry}</Text>
			</View>*/}

			<View style={styles.container}>
				<BarChart
					style={styles.chart}
					xAxis={localState.xAxis}
					yAxis={localState.yAxis}
					data={localState.data}
					doubleTapToZoomEnabled={false}
					legend={localState.legend}
					drawValueAboveBar={false}
					chartDescription={{ text: '' }}
					marker={{
						enabled: true,
						markerColor: processColor(colors.dark),
						textColor: processColor('white'),
						markerFontSize: 14,
					}}
					highlights={localState.highlights}
					// onSelect={handleSelect.bind(this)}
					// onChange={(event) => console.log(event.nativeEvent)}
				/>
			</View>

		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#F5FCFF'
	},
	chart: {
		flex: 1
	}
});


export default StackedBarChart;