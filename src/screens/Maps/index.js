import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import MapboxGL from '@react-native-mapbox-gl/maps';

MapboxGL.setAccessToken('pk.eyJ1IjoicmVzdGlha2EiLCJhIjoiY2tkZmU4eDFmMWN3cjJ5bXQ5ZzNsc2EwNSJ9.0p7AhCCxDKUxqwN5bqtFfg')

const Maps = () => {
	const [ coordinates, setCoordinates ] = useState([
						    [110.373528, -7.768053, 'RSUP Dr. Sardjito'],
						])

	useEffect(() => {
		const getLocation = async() => {
			try {
				const permission = await MapboxGL.requestAndroidLocationPermissions()
			} catch(err) {
				console.log(err)
			}
		}
		getLocation()
	}, [])

	const Markers = () => {
		return coordinates.map((data, i) => {
			let [long,lat,locationName] = data
			return (
				<MapboxGL.PointAnnotation id={"pointAnnotation"+i} coordinate={[long,lat]} key={i} >
					<MapboxGL.Callout title={locationName+"\nLong : "+long+"\nLat : "+lat}  key={i} />
				</MapboxGL.PointAnnotation>
			)
		})
	}

	return (
		<MapboxGL.MapView
			style={{flex:1}}
		>
			<MapboxGL.UserLocation
				visible={true}
			/>
			<MapboxGL.Camera
				followUserLocation={true}
			/>
			<Markers />
		</MapboxGL.MapView>
	)
}

export default Maps;