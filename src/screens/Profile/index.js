import React, { useState, useEffect, useContext } from 'react';
import { StyleSheet, Text, View, Image, Modal, StatusBar, TextInput, TouchableOpacity } from 'react-native';
import styles from '../../style';
import colors from '../../style/colors';
import Tombol from '../../components/Tombol';
import { RNCamera } from 'react-native-camera';
import Icon from 'react-native-vector-icons/Feather';
import storage from '@react-native-firebase/storage';
import auth from '@react-native-firebase/auth';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-community/async-storage';
import { GoogleSignin, statusCodes, GoogleSigninButton } from '@react-native-community/google-signin';
import { RootContext } from '../../context';

function Profile({ route, navigation }) {

    const state = useContext(RootContext)
	let camera
	const [ isVisible, setIsVisible ] = useState(false)
	const [ type, setType ] = useState('back')
	const [ photo, setPhoto ] = useState(null)
	const [ userInfo, setUserInfo ] = useState(null)

	useEffect(() => {
		getCurrentUser()
	}, [])

	const getCurrentUser = async () => {
		try {
			const userInfo = await GoogleSignin.signInSilently()
			setUserInfo(userInfo)
			const arr = {uri : userInfo && userInfo.user && userInfo.user.photo}
			saveLocalPicture(arr)
		} catch(err) {
			console.log(err)
		}
	}

	const onLogoutPress = async () => {
		try {
			if(userInfo) {
				await GoogleSignin.revokeAccess()
				await GoogleSignin.signOut()
			}
			auth().signOut()
		    .then((res) => {
		    })
		    .catch((err) => {
		        // alert(err)
		    })
		    state.changeStatusLoginChat('false')
			AsyncStorage.removeItem('token')
			AsyncStorage.removeItem('isLoginChat')
            AsyncStorage.removeItem('isLogin')
			navigation.reset({
                index: 0,
                routes: [{ name: 'Login' }],
            });
		} catch(err) {
			console.log(err)
		}
	}

	const toggleCamera = () => {
		setType(type === 'back' ? 'front' : 'back')
	}

	const takePicture = async () => {
		const options = { quality: 0.5, base64: true }
		if(camera) {
			const data = await camera.takePictureAsync(options)
			setPhoto(data)
			saveLocalPicture(data)
			setIsVisible(false)
		}
	}

	const saveLocalPicture = async (data) => {
		await AsyncStorage.setItem('local_pp',JSON.stringify(data))
	}

	const uploadImage = (uri) => {
		if(userInfo) {
			const sessionId = new Date().getTime()
			return storage()
			.ref(`images/${sessionId}`)
			.putFile(uri)
			.then((response) => {
				alert('Upload Success')
			})
			.catch((error) => {
				alert(error)
			})
		} else {
			alert('Perubahan berhasil disimpan di AsyncStorage tanpa proses upload foto ke firebase storage karena Anda login dengan fitur JWT')
		}
	}

	const renderCamera = () => {
		return (
			<Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
				<View style={{flex: 1}}>
					<RNCamera
						style={{flex: 1, justifyContent: 'space-around', flexDirection: 'column'}}
						ref={ref => {camera = ref;}}
						type={type}
					>
						<View style={styles.round} >
							<Text style={{color:'white'}}>{"Posisikan wajah"}</Text>
							<Text style={{color:'white'}}>{"di dalam"}</Text>
							<Text style={{color:'white'}}>{"lingkaran"}</Text>
						</View>
						<View style={styles.rectangle} >
							<Text style={{color:'white'}}>Kartu Peserta</Text>
						</View>
						<View style={{flexDirection: 'row',justifyContent: 'center',}}>
							<View style={{width: 150,height: 70,alignItems: 'flex-end'}}>
								<TouchableOpacity style={styles.btnTake} onPress={() => takePicture()}>
									<Icon name="camera" size={30} />
								</TouchableOpacity>
							</View>
							<View style={{justifyContent: 'center', height: 70,width: 80,alignItems: 'flex-end',}}>
								<TouchableOpacity style={styles.btnFlip} onPress={() => toggleCamera()}>
									<MaterialCommunity name="rotate-3d-variant" size={15} />
								</TouchableOpacity>
							</View>
						</View>
					</RNCamera>
				</View>
			</Modal>
		)
	}

	return (
		<View style={styles.container}>
			<StatusBar backgroundColor="#203339" barStyle="light-content" />
			{renderCamera()}
			<View style={styles.header}>
				{(userInfo) ?
					<Image style={styles.profilePicture} source={photo === null ? {uri: userInfo && userInfo.user && userInfo.user.photo} : { uri: photo.uri }} />
					:
					<Image style={styles.profilePicture} source={photo === null ? require('../../assets/images/logo.jpg') : { uri: photo.uri }} />
				}
				<TouchableOpacity onPress={() => setIsVisible(true)}>
					<Text style={styles.userName}>Change Picture</Text>
				</TouchableOpacity>
			</View>
			<View style={[styles.body,{height: 430}]}>
				<View style={styles.line}>
					<Text style={[styles.formLabel,{fontWeight: 'bold'}]}>Nama</Text>
                    <TextInput
                        placeholder='Nama'
                        value={userInfo && userInfo.user && userInfo.user.name}
                        style={styles.textInput}
                    />
				</View>
				<View style={styles.line}>
					<Text style={[styles.formLabel,{fontWeight: 'bold'}]}>Email</Text>
                    <TextInput
                        placeholder='Email'
                        value={userInfo && userInfo.user && userInfo.user.email}
                        style={styles.textInput}
                    />
				</View>
				<View style={styles.line}>
					<Text style={[styles.formLabel,{fontWeight: 'bold'}]}>Password</Text>
                    <TextInput
                        placeholder='Password'
                        style={styles.textInput}
                    />
				</View>
				<View style={{height: 150}}>
					<Tombol onPress={() => uploadImage(photo.uri)} title={"Simpan Perubahan"} />
					<Tombol bgColor={colors.danger} onPress={() => onLogoutPress()} title={"Logout"} />
				</View>
			</View>
		</View>
	)
}

export default Profile;