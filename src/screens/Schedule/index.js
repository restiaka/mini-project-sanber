import React, { useState, createContext } from 'react';
import ScheduleList from './Schedule';
import AsyncStorage from '@react-native-community/async-storage';
import { RootContext } from '../../context';

const Context = () => {

	const [ asal, setAsal ] = useState('')
	const [ tujuan, setTujuan ] = useState('')
	const [ schedules, setSchedules ] = useState([])
	const [ refresh, setRefresh ] = useState(true)

	React.useEffect(() => {
        async function getSavedSchedule() {
            const saved_schedule = await AsyncStorage.getItem('saved_schedule')
            if (saved_schedule !== null) {
            	setSchedules(JSON.parse(saved_schedule))
            }
        }
        getSavedSchedule()
    }, [])

	const handleChangeInputAsal = (value) => {
		setAsal(value)
	}

	const handleChangeInputTujuan = (value) => {
		setTujuan(value)
	}

	const addSchedule = () => {
		if (asal === '') {
			alert('Inputkan asal rute Anda')
		} else if (tujuan === '') {
			alert('Inputkan tujuan rute Anda')
		} else {
			let newSchedules = [...schedules, {asal: asal, tujuan: tujuan}]
			saveSchedule(newSchedules)
			setSchedules(newSchedules)
			// setTujuan('')
			setAsal('')
		}
	}
	
	const saveSchedule = (newSchedules) => {
		AsyncStorage.setItem('saved_schedule',JSON.stringify(newSchedules))
	}

	const deleteSchedule = (index) => {
		schedules.splice(index, 1)
		saveSchedule(schedules)
		setRefresh(!refresh)
	}

	return (
		<RootContext.Provider value={{ asal, schedules, handleChangeInputAsal, handleChangeInputTujuan, addSchedule, deleteSchedule, refresh }}>
			<ScheduleList />
		</RootContext.Provider>
	)
}

export default Context;