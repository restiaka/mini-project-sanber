import React, { useContext } from 'react';
import { StyleSheet, StatusBar, View, Text, TouchableOpacity, FlatList, TextInput } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Icon from 'react-native-vector-icons/AntDesign';
import { RootContext } from '../../context';
import Tombol from '../../components/Tombol';
import styles from '../../style';

const ScheduleList = () => {
	const state = useContext(RootContext)

	const renderItem = ({item,index}) => {
		return (
			<View style={local_styles.list}>
				<View>
					<Text>{item.asal}</Text>
					<Text>{item.tujuan}</Text>
				</View>
				<TouchableOpacity style={local_styles.deleteButton} onPress={() => state.deleteSchedule(index)}>
					<Icon name="delete" size={25} />
				</TouchableOpacity>
			</View>
		)
	}

	return (
		<View style={styles.container}>
			<StatusBar backgroundColor="#203339" barStyle="light-content" />
			<View style={local_styles.formContainer}>
				<Text style={{marginBottom: 10,fontWeight: 'bold',fontSize: 30,alignSelf: 'center'}}>Rute Gowes Personal</Text>
				<View>
					<Text style={styles.formLabel}>Asal</Text>
					<TextInput
						style={styles.textInput}
						value={state.asal}
						placeholder="Inputkan lokasi start"
						onChangeText={(asal) => state.handleChangeInputAsal(asal)}
					/>
				</View>
				<View>
					<Text style={styles.formLabel}>Tujuan</Text>
					<TextInput
						style={styles.textInput}
						value={state.tujuan}
						placeholder="Inputkan lokasi finish"
						onChangeText={(tujuan) => state.handleChangeInputTujuan(tujuan)}
					/>
				</View>
				<Tombol onPress={() => state.addSchedule()} title={"Tambah"} />
			</View>
			<View style={local_styles.listContainer}>
				<FlatList
					data={state.schedules}
					renderItem={renderItem}
					keyExtractor={(item,index)=>index.toString()}
					extraData={state.refresh}
				/>
			</View>
		</View>
	)

}

const local_styles = StyleSheet.create({
	formContainer: {
		marginTop: 10,
		padding: 20,
		height: 300,
	},
	listContainer: {
		flex: 1,
		paddingHorizontal: 10
	},
	list: {
		padding: 20,
	    paddingRight: 100,
	    borderWidth: 5,
	    borderRadius: 5,
	    borderColor: '#ededed',
	    marginBottom: 10
	},
	deleteButton: {
		position: 'absolute',
	    justifyContent: 'center',
	    alignItems: 'center',
	    padding: 10,
	    top: 10,
	    bottom: 10,
	    right: 10
	}
});

export default ScheduleList;