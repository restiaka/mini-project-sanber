import React from 'react';

const colors = {
	info: '#ffc100',
	primary: '#add7f6',
	warning: '#ff8200',
	danger: '#ff0000',
	dark: '#203339',
	success: 'green',
}

export default colors