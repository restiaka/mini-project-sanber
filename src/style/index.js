import React, { useContext } from 'react';
import { StyleSheet } from 'react-native';
import colors from './colors';

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: 'white',
	},
	buttonCircle: {
		width: 50,
		height: 50,
		borderRadius: 25,
		borderStyle: 'solid',
		backgroundColor: '#16166c',
		alignItems: 'center',
		justifyContent: 'center',
	},
	slide: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
	},
	title: {
		fontSize: 24,
		color: '#16166c',
		fontWeight: 'bold',
	},
	image: {
		height: 250,
		width: 250,
		marginVertical: 50,
	},
	text: {
		paddingHorizontal: 10,
		fontSize: 15,
		fontWeight: 'bold',
		color: '#a4a7a6',
		textAlign: 'center',
	},
	loginHeader: {
		height: 250,
		justifyContent: 'space-around',
		alignItems: 'center',
	},
	loginBody: {
		height: 350,
		paddingHorizontal: 20,
	},
	loginFooter: {
		justifyContent: 'center',
		flexDirection: 'row',
		padding: 20,
	},
	textInput: {
		borderBottomWidth: 2,
		borderBottomColor: '#eee',
		borderStyle: 'solid',
		marginBottom: 20,
	},
	loginPage: {
		flex: 1,
		justifyContent: 'space-between'
	},
	hr: {
		marginVertical: 10,
		height: 1,
		borderWidth: 1,
		borderColor: '#ddd',
		borderStyle: 'solid',
	},
	or: {
		color: '#000',
		alignSelf: 'center',
		textAlign: 'center',
		marginTop: -20,
		marginBottom: 5,
		backgroundColor: '#fff',
		width: 30
	},
	header: {
		backgroundColor: '#203339',
		alignItems: 'center',
		height: 200,
		borderBottomLeftRadius: 50,
		borderBottomRightRadius: 50,
		justifyContent: 'center',
	},
	profilePicture: {
		width: 100,
		height: 100,
		borderRadius: 50,
		borderWidth: 3,
		borderColor: 'white',
	},
	userName: {
		color: 'white',
		fontWeight: 'bold',
		fontSize: 20,
		paddingTop: 15
	},
	body: {
		marginTop: -25,
		marginHorizontal: 20,
		padding: 20,
		borderRadius: 10,
		backgroundColor: 'white',
		height: 200,
		elevation: 3,
	},
	btnFlip: {
		backgroundColor: 'white',
		width: 40,
		height: 40,
		borderRadius: 20,
		justifyContent: 'center',
		alignItems: 'center'
	},
	round: {
		marginTop: 150,
		alignSelf: 'center',
		width: 200,
		height: 250,
		borderRadius: 150,
		borderWidth: 2,
		borderColor: 'white',
		borderStyle: 'solid',
		justifyContent: 'flex-end',
		alignItems: 'center',
		paddingBottom: 10
	},
	rectangle: {
		alignSelf: 'center',
		width: 200,
		height: 100,
		borderWidth: 2,
		borderColor: 'white',
		borderStyle: 'solid',
		marginTop: 20,
		alignItems: 'center'
	},
	btnTake: {
		backgroundColor: 'white',
		width: 70,
		height: 70,
		borderRadius: 35,
		justifyContent: 'center',
		alignItems: 'center',
		marginBottom: 20
	},
	row: {
		flexDirection: 'row',
		alignItems: 'center',
	},
	btnFinger: {
		height: 50,
		padding: 5,
		borderWidth: 2,
		borderColor: '#203339',
		borderStyle: 'solid',
		borderRadius: 5,
	},
	photoCircle: {
		borderWidth: 3,
		borderColor: '#203339',
		borderStyle: 'solid',
		borderRadius: 70,
		width: 140,
		height: 140,
		justifyContent: 'center',
		alignItems: 'center'
	},
	btnLogoutChat: {
		borderRadius: 5,
		backgroundColor: colors.danger,
		width: 55,
		justifyContent: 'center',
		alignItems: 'center',
		paddingLeft: 5,
		margin: 5,
	}
})

export default styles