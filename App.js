import React, { useEffect } from 'react';
import Navigation from './src/navigation';
import firebase from '@react-native-firebase/app';
import codePush from 'react-native-code-push';
import OneSignal from 'react-native-onesignal';
// import analytic from '@react-native-firebase/analytics';

// // Your web app's Firebase configuration
var firebaseConfig = {
	apiKey: "AIzaSyA4tJihYmAfO3EVyVq1kx7A3_ROWai1S-Q",
	authDomain: "insgo-e1a12.firebaseapp.com",
	databaseURL: "https://insgo-e1a12.firebaseio.com",
	projectId: "insgo-e1a12",
	storageBucket: "insgo-e1a12.appspot.com",
	messagingSenderId: "472949987951",
	appId: "1:472949987951:web:e8bc7b4e587389b9348b9a",
	measurementId: "G-CP1H7LGNEL"
};

// // Inisialisasi firebase
if(!firebase.apps.length) {
	firebase.initializeApp(firebaseConfig)
	// analytic.analytics();
}
const App = () => {
	useEffect(() => {
	    OneSignal.setLogLevel(6, 0);

	    OneSignal.init("5a97b362-1239-4605-9346-6c55cb6bfd82", {kOSSettingsKeyAutoPrompt : false, kOSSettingsKeyInAppLaunchURL: false, kOSSettingsKeyInFocusDisplayOption:2});
	    OneSignal.inFocusDisplaying(2);

	    // OneSignal.addEventListener('received', onReceived)
	    // OneSignal.addEventListener('opened', onOpened)
	    // OneSignal.addEventListener('ids', onIds)

	    codePush.sync({
	    	updateDialog: true,
	    	intallMode: codePush.InstallMode.IMMEDIATE
	    }, SyncStatus)

	    // return () => {
	    // 	OneSignal.removeEventListener('received', onReceived)
	    // 	OneSignal.removeEventListener('opened', onOpened)
	    // 	OneSignal.removeEventListener('ids', onIds)
	    // }
	}, [])

	const SyncStatus = (status) => {
		switch (status) {
			case codePush.SyncStatus.CHECKING_FOR_UPDATE:
				console.log('Checking for Update')
				break;
			case codePush.SyncStatus.DOWNLOADING_PACKAGE:
				console.log('Downloading Package')
				break;
			case codePush.SyncStatus.UP_TO_DATE:
				console.log('Up to date')
				break;
			case codePush.SyncStatus.INSTALLING_UPDATE:
				console.log('Installing Update')
				break;
			case codePush.SyncStatus.UPDATE_INSTALLED:
				console.log("Notification", "Update Installed")
				break;
			case codePush.SyncStatus.AWAITING_USER_ACTION:
				console.log('Awaiting User')
				break;
			default:
				break;
		}
	}
	
    return (
        <Navigation />
    )
}

export default App;